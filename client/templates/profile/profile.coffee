Template.profile.helpers
  locations: () ->
    Locations.find()
  userEmail: ()->
    Meteor.user().emails[0].address

Template.profile.events
  'submit #update-user': (e, t)->
    e.preventDefault()
    name = $(e.target).find('#name').val()
    locationId = $(e.target).find('#location').val()
    email = $(e.target).find('#email').val()
    Meteor.users.update { _id: Meteor.userId() }, 
      $set:
        'profile.name': name,
        'profile.locationId': locationId
    Meteor.call 'user/changeEmail', email