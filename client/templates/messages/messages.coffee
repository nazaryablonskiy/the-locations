Template.messages.helpers
  location: () ->
    Locations.findOne Meteor.user().profile.locationId
  messages: ()->
    Messages.find {locationId: Meteor.user().profile.locationId}