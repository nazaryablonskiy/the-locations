Accounts.ui.config
  requestPermissions: {}
  extraSignupFields: [
    {
      fieldName: 'name'
      fieldLabel: 'First name'
      inputType: 'text'
      visible: true
      validate: (value, errorFunction) ->
        if !value
          errorFunction 'Please write your first name'
          false
        else
          true
    }
    {
      fieldName: 'locationId'
      fieldLabel: 'Location'
      inputType: 'select'
      showFieldLabel: true
      empty: 'Please select your location'
      data: ()->
        Locations.find().map (loc)->
          {
            id: loc._id
            value: loc._id
            label: loc.name
          }
      visible: true
      validate: (value, errorFunction) ->
        if !value
          errorFunction 'Please choose your location'
          false
        else
          true
    }
  ]
