Meteor.publish 'locations', ()->
    Locations.find()

Meteor.publish 'messages', ()->
  if @.userId
    user = Meteor.users.findOne this.userId
    Messages.find locationId: user.profile.locationId

Meteor.publish 'users', ()->
  if @.userId
    user = Meteor.users.findOne this.userId
    Meteor.users.find locationId: user.profile.locationId

Meteor.publish 'locationUsers', ()->
  if @.userId
    user = Meteor.users.findOne this.userId
    Meteor.users.find {'profile.locationId': user.profile.locationId}