Meteor.methods
  'user/changeEmail': (email)->
    if @.userId
      Meteor.users.update {_id: @.userId},
        $set:
          'emails': [{address: email, verified: false}]