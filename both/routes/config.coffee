Router.configure
  layoutTemplate: 'layout'

Router.onBeforeAction ()->
  if (not Meteor.userId())
    @.render 'signInPlease'
  @.next()

Router.route '/',
  name: 'messages'
  waitOn: ()->
    if Meteor.userId()
      [
        Meteor.subscribe('locations'),
        Meteor.subscribe('messages'),
        Meteor.subscribe('users'),
        Meteor.subscribe('locationUsers')
      ]

Router.route '/profile',
  name: 'profile'
  waitOn: ()->
    [
      Meteor.subscribe('locations')
    ]
