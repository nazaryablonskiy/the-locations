@Messages = new Mongo.Collection 'messages'

MessageSchema = new SimpleSchema
  text: 
    type: String
    label: 'Text'
    max: 300
  ownerId:
    type: String,
    autoValue: ()->
      this.userId
    autoform:
      type: 'hidden'
  createdAt: 
    type: Date
    autoValue: ()->
      new Date()
    autoform:
      type: 'hidden'
  locationId:
    type: String,
    autoValue: ()->
      user = Meteor.users.findOne this.userId
      user.profile.locationId
    autoform:
      type: 'hidden'

Messages.attachSchema MessageSchema

Messages.allow
  insert: (userId, doc) ->
    userId
  update: (userId, doc, fields, modifier) ->
    userId == doc.ownerId
  remove: (userId, doc) ->
    userId == doc.ownerId
